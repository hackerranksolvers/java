package com.hackerrank.introduction;

import java.util.Scanner;

public class JavaEndOfFile {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int count = 1;
        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            System.out.println(count + " " + line);
            count++;
        }
        scanner.close();
    }

}
