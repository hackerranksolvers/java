package com.hackerrank.introduction;

import java.util.Scanner;

public class JavaOutputFormatting {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("================================");
        for (int i = 0; i < 3; i++) {
            String s = scanner.next();
            int n = scanner.nextInt();
            System.out.printf("%-15s%03d\n", s, n);
        }
        System.out.println("================================");
        scanner.close();
    }

}
