package com.hackerrank.introduction;

import java.util.Scanner;

public class JavaLoops2 {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int q = scanner.nextInt();

        for (int i = 0; i < q; i++) {
            int a = scanner.nextInt();
            int b = scanner.nextInt();
            int n = scanner.nextInt();
            process(a, b, n);
        }
        scanner.close();
    }

    private static void process(int x, int y, int z) {
        for (int i = 0; i < z; i++) {
            int temp = x;
            for (int j = 0; j <= i; j++) {
                temp += (y * Math.pow(2, j));
            }
            System.out.print(temp + " ");
        }
        System.out.println();
    }

}
