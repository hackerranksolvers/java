package com.hackerrank.introduction;

import java.util.Scanner;

public class JavaIfElse {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.close();

        System.out.println(process(n));
    }

    private static String process(int a) {
        if (a % 2 != 0) {
            return "Weird";
        } else if ((2 <= a) && (a <= 5)) {
            return "Not Weird";
        } else if ((6 <= a) && (a <= 20)) {
            return "Weird";
        }
        return "Not Weird";
    }

}
