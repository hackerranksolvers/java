package com.hackerrank.introduction;

import java.util.Scanner;

public class JavaDatatypes {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int t = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < t; i++) {
            String s = scanner.nextLine();
            try {
                long l = Long.parseLong(s);
                System.out.println(l + " can be fitted in:");
                process(l);
                System.out.println("* long");
            } catch (Exception e) {
                System.out.println(s + " can't be fitted anywhere.");
            }
        }

        scanner.close();
    }

    private static void process(long x) {
        if ((Byte.MIN_VALUE <= x) && (x <= Byte.MAX_VALUE)) {
            System.out.println("* byte");
        }
        if ((Short.MIN_VALUE <= x) && (x <= Short.MAX_VALUE)) {
            System.out.println("* short");
        }
        if ((Integer.MIN_VALUE <= x) && (x <= Integer.MAX_VALUE)) {
            System.out.println("* int");
        }
    }

}
