package com.hackerrank.introduction;

import java.util.Scanner;

public class JavaLoops1 {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        process(n);
        scanner.close();
    }

    private static void process(int a) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(a + " x " + i + " = " + a * i);
        }
    }

}
